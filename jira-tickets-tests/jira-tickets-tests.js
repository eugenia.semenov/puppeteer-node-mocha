const puppeteer = require('puppeteer');
const delay = require('await-delay');
const forEach = require('mocha-each');
const _ = require('underscore');
const testCases = require('../testcase-list.js');
const validatePing = require('../validate-pings.js');
const baseFunction = require('../base-functions');


describe('BSDK Defects verification', function () {
	let browser;

	after('close browser', async function () {
		await browser.close();
		console.log('Test complete');
	});

	forEach(testCases.test)

		.it('Testing jira tickets %s for %d milliseconds', async function (testName, testDuration) {
				let output = [];
				let testEnvOptions = '';
				let page;
				let pages;
				let testFolder = 'jira-tickets-tests/';

				browser = await puppeteer.launch({ headless: true });
				pages = await browser.pages();
				page = pages[0];
				await page.setRequestInterception(true);

				page.on('request', request => {
					let url = request.url(), obj;
					obj = baseFunction.urlToJson(url);
					if (obj && Object.keys(obj).length > 0 && url.includes('mtvr') || url.includes('trans') || url.includes('tsv') || url.includes('cgi-bin/gn')) {
						request.continue();
						output.push(obj);
					} else { request.continue(); }
				});

				testEnvOptions = await baseFunction.checkEnvOptions(testName);
				await page.goto(testEnvOptions), { waitUntil: 'domcontentloaded' };
				await delay(4000);
				await baseFunction.waitTestComplete(page, testDuration);
				await validatePing.validateTestResult(output, testName, testEnvOptions, testFolder);
				await browser.close();
		})
});