const chai = require('chai');
const assert = chai.assert;
const delay = require('await-delay');
const _ = require('underscore');
const args = require('yargs').argv;
const fs = require('fs');


module.exports = {

    urlToJson: function url2Json(txtUrl) {
        let obj = {};

        if (txtUrl.split('?')[1]) {
            obj._baseurl = txtUrl.split('?')[0];
            obj._sentAt = new Date();
            txtUrl.split('?')[1].split('&').forEach(function (itm) {
                obj[itm.split('=')[0]] = itm.split('=')[1];
            });
        }
        return obj;
    },

    checkEnvOptions: async function checkOptions(testName) {
        let baseUrl = 'http://engtestsite.com/bsdk/testsuite/testharness/single/index_eugenia.html?env=qat4&file=selectFile&cdn=static-staging1&nsdkv=bndl&configEnv=static-staging2&ggcmbVersion=bundled600';
        let selectEnv = ['qat3', 'qat4', 'dcr', 'eu', 'us', 'it', 'pl', 'au'];
        let selectCdn = ['development', 'development2', 'static-staging1', 'static-staging2', 'futuremaster', 'cdn-gl'];
        let selectBundle = [600, 601, '600_rc', '601_rc', '600.eu', '600.au'];
        let selectConfig = ['static-staging1', 'static-staging2', 'futuremaster', 'prod'];

        if (_.contains(selectEnv, args.env) && _.contains(selectCdn, args.cdn) && _.contains(selectBundle, args.bundle) && _.contains(selectConfig, args.conf)) {
            testUrl = baseUrl.replace('selectFile', testName).replace('qat4', args.env).replace('static-staging1', args.cdn).replace('bndl', args.bundle).replace('static-staging2', args.conf);
            return testUrl;
        } else {
            assert.fail('Test enviromnent options selected incorrectly! ' + 'Env is ' + args.env + '. CDN is ' + args.cdn + '. Bundle is ' + args.bundle + '. Config is ' + args.conf + '.');
        }
        await delay(3000);
    },

    waitTestComplete: async function waitTestComplete(page, testDuration) {
        let pageUrl = 'http://engtestsite.com/bsdk/testsuite/testharness/single/';

        try {
            await page.waitForSelector('#runBtn:not([disabled])', { timeout: testDuration });
            await delay(3000);
            await page.goto(pageUrl);
            await delay(3000);
        } catch (err) {
            assert.fail(err);
        }
    },

    saveBaseline: function saveBaseFile(output, testName, testEnvOptions, testFolder, testNameJson) {
        let baselineDate = new Date();
        let baselineDirectory = testFolder + '/_baseline/';
        let testResultBaseline = { 'testCaseName': testName, 'baselineGeneratedDate': baselineDate, 'testUrl': testEnvOptions, 'results': output };

        if (fs.existsSync(baselineDirectory)) {
            fs.writeFile(baselineDirectory + testNameJson, JSON.stringify(testResultBaseline, null, '\t'), 'utf8', function (err) {
                if (err) {
                    console.log(err);
                }
            });
        } else {
            fs.mkdirSync(baselineDirectory, { recursive: true });
            fs.writeFile(baselineDirectory + testNameJson, JSON.stringify(testResultBaseline, null, '\t'), 'utf8', function (err) {
                if (err) {
                    console.log(err);
                }
            });
        }
    },

    saveCurrent: function saveCurrentFile(output, testName, testEnvOptions, testFolder, testNameJson) {
        let currentResultDate = new Date();
        let currentDirectory = testFolder + '/_current/';
        let testResultCurrent = { 'testCaseName': testName, 'resultGeneratedDate': currentResultDate, 'testUrl': testEnvOptions, 'results': output };

        if (fs.existsSync(currentDirectory)) {
            fs.writeFile(currentDirectory + testNameJson, JSON.stringify(testResultCurrent, null, '\t'), 'utf8', function (err) {
                if (err) {
                    console.log(err);
                }
            });
        } else {
            fs.mkdirSync(currentDirectory, { recursive: true });
            fs.writeFile(currentDirectory + testNameJson, JSON.stringify(testResultCurrent, null, '\t'), 'utf8', function (err) {
                if (err) {
                    console.log(err);
                }
            });
        }
    },
    
    saveResult: function saveResultFile(testName, testEnvOptions, finalResult, testFolder, testNameJson) {
        let finalResultDate = new Date();
        let resultsDirectory = testFolder + '/_results/';
        let testResultFinal = { 'testCaseName': testName, 'comparisonResultDate': finalResultDate, 'testUrl': testEnvOptions, 'results': finalResult };

        if (fs.existsSync(resultsDirectory)) {
            fs.writeFile(resultsDirectory + testNameJson, JSON.stringify(testResultFinal, null, '\t'), 'utf8', function (err) {
                if (err) {
                    console.log(err);
                }
            });
        } else {
            fs.mkdirSync(resultsDirectory, { recursive: true });
            fs.writeFile(resultsDirectory + testNameJson, JSON.stringify(testResultFinal, null, '\t'), 'utf8', function (err) {
                if (err) {
                    console.log(err);
                }
            });
        }
    }
};