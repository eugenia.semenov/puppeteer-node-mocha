const fs = require('fs');
const chai = require('chai');
const assert = chai.assert;
const args = require('yargs').argv;
const delay = require('await-delay');
const validate = require('validate.js');
const baseFunction = require('./base-functions.js');


function compareResult(current, base) {
  // These parameters will never match.
  let ignore = ['trfd', 'sup', 'sessionId', 'c1', 'c29', 'c30', 'si', 'c64', 'c59', 'c61', 'c62', 'rnd', 'evdata', '_sentAt', 'pli', 'tp'];
  let comparisonResult = {};
  // compare fields in current but not in base.
  for (let fld in current) {
    if (fld == '_baseurl' && current[fld] == base[fld]) comparisonResult['url'] = current[fld];
    if (ignore.indexOf(fld) == -1) {
      if (typeof base[fld] == 'undefined') {
        comparisonResult._additionalParam = 'Param(s) below is not found in baseline';
        comparisonResult[fld] = 'additional param sent';
      } else if (current[fld] != base[fld]) {
        comparisonResult._notEqualParamValue = 'Param(s) below has incorrect value';
        comparisonResult[fld] = 'pings are not equal: ' + 'Actual: ' + current[fld] + '.' + ' Expected: ' + base[fld];
      }
    }
  }

  // compare fields in base but not in current.
  for (let fld in base) {
    if (ignore.indexOf(fld) == -1) {
      if (typeof current[fld] == 'undefined') {
        comparisonResult._missing = 'Param(s) below is missing from current bundle';
        comparisonResult[fld] = 'missing param from current bundle';
      }
    }
  }

  // Remove _sentAt field if _baseUrl is the same.
  if (!comparisonResult['_baseurl']) {
    delete comparisonResult['_sentAt'];
  }

  return comparisonResult;
};

function comparePings(testFolder, testName, testEnvOptions, testNameJson) {
  let result, finalResult = [];
  
  let baselineData = JSON.parse(fs.readFileSync(testFolder + '/_baseline/' + testNameJson));
  let currentData = JSON.parse(fs.readFileSync(testFolder + '/_current/' + testNameJson));

  // Check counts of pings created.
  if (currentData.results.length != baselineData.results.length) {
    finalResult.push({ _numberOfPings: '# of pings sent are different.' + ' Expected ' + baselineData.results.length + ' ping(s).' + '  Actual ' + currentData.results.length + ' ping(s)' });
  }
  //Compare pings.
  for (let i = 0; i < currentData.results.length; i++) {
    if (currentData.results[i] && baselineData.results[i]) {
      result = compareResult(currentData.results[i], baselineData.results[i]);
      if (Object.keys(result).length > 0) {
        finalResult.push(result);
      }
    } else {
      if (finalResult[i]) {
        finalResult.push({ _missing: 'ping # ' + (i + 1) + ' missing from baseline.' });
      } else {
        finalResult.push({ _missing: 'ping # ' + (i + 1) + ' missing from current.' });
      }
    }
  }

  baseFunction.saveResult(testName, testEnvOptions, finalResult, testFolder, testNameJson);

 };

function checkResult(testFolder, testNameJson) {
  let finalResultFile = JSON.parse(fs.readFileSync(testFolder + '/_results/' + testNameJson));

  for (let i = 0; i < finalResultFile.results.length; i++) {
    if ('_missing' in finalResultFile.results[i] || '_numberOfPings' in finalResultFile.results[i] || '_additionalParam' in finalResultFile.results[i] || '_notEqualParamValue' in finalResultFile.results[i]) {
      return finalResultFile;
    }
  }
};

function applyAssert(testFolder, testNameJson) {
  let checkFinalResult = checkResult(testFolder, testNameJson);

  if (checkFinalResult) {
    console.log('Pings are not equal!');
    assert.fail(JSON.stringify(checkFinalResult, null, 2));
  } else {
    console.log('Params in pings are equal');
  }
};

module.exports = {

  validateTestResult: async function validateResult(output, testName, testEnvOptions, testFolder) {
    let testNameJson = testName.replace('xml', 'json');

    if (output && output.length === 0) {
      assert.fail('Result hasn\'t been generated. Most likely there is a problem with testharness test page');
    }

    if (args.testRun == 'baseline') {
      console.log('Creating baseline');
      baseFunction.saveBaseline(output, testName, testEnvOptions, testFolder, testNameJson);
    } else {
      console.log('Comparing current result against baseline');
      baseFunction.saveCurrent(output, testName, testEnvOptions, testFolder, testNameJson);
      await delay(3000);
      comparePings(testFolder, testName, testEnvOptions, testNameJson);
      await delay(3000);
      applyAssert(testFolder, testNameJson);
    }
  }
};
