module.exports = {
    dcrvideoSmokeTest: [
        ['Smoke_GGPM_DCR_Video_305secs.xml', 350000],
        ['Smoke_GGPM_DCR_Video_AllAds_WithEvent7_240sec.xml', 280000],
        ['Smoke_GGPM_DCR_Video_ChannelChange_90sec.xml', 120000],
        ['Smoke_GGPM_DCR_Video_Rewind_Fastforward_180sec.xml', 210000]
        ],
    dcrstaticSmokeTest: [
        ['Smoke_GGPM_DCR_Static_305sec_refresh.xml', 330000],
        ['Smoke_GGPM_DCR_Static_ChannelChange_60sec.xml', 80000]
        ],
    dtvrSmokeTest: [
        ['Smoke_GGPM_DTVR_IVD_play300sec.xml', 330000],
        ['Smoke_GGPM_DTVR_PCFDChannelChange_120sec.xml', 160000],
        ['Smoke_GGPM_DTVR_Sping_20sec.xml', 30000],
        ['Smoke_GGPM_Hybrid_DCR_DTVR_samePCFD_event57_120sec.xml', 160000]
    ],
    dtvrGgpmRegression: [
        ['REGRESSION_BSDK_GGPM_DTVR_Admodel_PCtoFDChannelChnage_Admodel1to2.xml', 150000],
        ['REGRESSION_BSDK_GGPM_DTVR_Admodel_withoutadmodelparam.xml', 400000],
        ['REGRESSION_BSDK_GGPM_DTVR_Live_Admodel100.xml', 400000],
        ['REGRESSION_BSDK_GGPM_DTVR_Live_AdmodelDynamic.xml', 350000],
        ['REGRESSION_BSDK_GGPM_DTVR_Live_AdmodelInvalid.xml', 380000],
        ['REGRESSION_BSDK_GGPM_DTVR_Live_AdmodelLinear.xml', 350000],
        ['REGRESSION_BSDK_GGPM_DTVR_Live_NoAQHQuarterCross.xml', 400000],
        ['REGRESSION_BSDK_GGPM_DTVR_Live_ShortPing.xml', 40000]
    ],       
    dtvrTrackEventRegression: [
        ['REGRESSION_BSDK_TrackEvent_DTVR_AQH_qual_not_met_fwd_pc.xml', 750000],
        ['REGRESSION_BSDK_TrackEvent_DTVR_AQH_qual_not_met_rewind_fd.xml', 750000],
        ['REGRESSION_BSDK_TrackEvent_DTVR_FirstMin_5Times.xml', 350000],
        ['REGRESSION_BSDK_TrackEvent_DTVR_PauseTimeOut_PCFD.xml', 2200000]
    ],    
    dtvrGgpmRtvodRegression: [
        ['REGRESSION_BSDK_GGPM_DTVR_RTVOD_Midstream_FD07_Play_pause.xml', 7800000],
        ['REGRESSION_BSDK_GGPM_DTVR_RTVOD_Midstream_FD07_to_PCnoFlag.xml', 425000],
        ['REGRESSION_BSDK_GGPM_DTVR_RTVOD_Midstream_FD07.xml', 650000],
        ['REGRESSION_BSDK_GGPM_DTVR_RTVOD_Midstream_PC03.xml', 680000],
        ['REGRESSION_BSDK_GGPM_DTVR_RTVOD_Midstream_PC07_to_FD03_Sping.xml', 58000],
        ['REGRESSION_BSDK_GGPM_DTVR_RTVOD_Midstream_PC07_to_FD03.xml', 950000],
        ['REGRESSION_BSDK_GGPM_DTVR_RTVOD_Midstream_PC07_to_PC03.xml', 670000],
        ['REGRESSION_BSDK_GGPM_DTVR_RTVOD_Midstream_PCFD07_to_PCFD07.xml', 650000]
    ],
    dtvrGgpmRtvodFwdFfRegression: [
        ['REGRESSION_BSDK_GGPM_DTVR_RTVOD_Midstream_PCFD03_fastForwardSameSegment.xml', 385000],
        ['REGRESSION_BSDK_GGPM_DTVR_RTVOD_Midstream_PCFD03_replayFirst5min.xml', 630000],
        ['REGRESSION_BSDK_GGPM_DTVR_RTVOD_Midstream_PCFD03_rewindDifferentSegment.xml', 480000],
        ['REGRESSION_BSDK_GGPM_DTVR_RTVOD_Midstream_PCFD07_fastforwardDiffSegment.xml', 390000],
        ['REGRESSION_BSDK_GGPM_DTVR_RTVOD_Midstream_PCFD07_rewindSameSegment.xml', 480000]
    ],
    dtvrGgpmRtvodChannelChangeRegression: [
        ['REGRESSION_BSDK_GGPM_DTVR_RTVOD_Midstream_ChannelChange_FD-noFlag_to_PCFD03.xml', 650000],
        ['REGRESSION_BSDK_GGPM_DTVR_RTVOD_Midstream_ChannelChange_PC_incorrectCID_to_FD03.xml', 400000],
        ['REGRESSION_BSDK_GGPM_DTVR_RTVOD_Midstream_ChannelChange_PCFD03_to_FD03.xml', 740000],
        ['REGRESSION_BSDK_GGPM_DTVR_RTVOD_Midstream_ChannelChange_PCFD03_to_PC1FD07.xml', 650000]
    ],
    defectPauseTimeout: [
        ['DV15963_GGPM_DCRVideo_Midroll_PauseTimeoutNOEvent7.xml', 82000],
        ['DV15963_GGPM_DCRVideo_Midroll_PauseTimeoutNOEvent7Event15.xml', 83000],
        ['DV15963_GGPM_DCRVideo_PauseTimeoutEvent7.xml', 75000],
        ['DV15963_GGPM_DCRVideo_PauseTimeoutNOEvent7.xml', 73000],
        ['DV15963_GGPM_DCRVideoStatic_PauseTimeout.xml', 72000],
        ['DV15963_GGPM_DCRVideoStatic_PauseTimeoutEvent7.xml', 71000],
        ['DV15963_GGPM_DCRVideoStaticMulti_PauseTimeoutAdsEvent7.xml', 140000],
        ['DV15963_GGPM_DTVR_PauseTimeout_Event7.xml', 117000],
        ['DV15963_GGPM_DTVR_PauseTimeout_NOEvent7.xml', 117000],
        ['DV15963_GGPM_Hybrid_DCR_DTVR_DCR_Event7.xml', 192000],
        ['DV15963_GGPM_Hybrid_DCR_DTVR_Event7.xml', 237000],
        ['DV15963_GGPM_Hybrid_DCR_DTVR_Event7_noPlayheads.xml', 115000],
    ],
    test: [
        ['Smoke_GGPM_DCR_Video_TEST_5sec.xml', 30000],
        ['Smoke_GGPM_DCR_Video_TEST_5sec.xml', 30000],
        ['Smoke_GGPM_DCR_Video_TEST_5sec.xml', 30000]
    ],
    newTests: [
        ['Smoke_GGPM_DCR_Video_TEST_5sec.xml', 350000],
        ['Smoke_GGPM_DCR_Static_TEST_5sec.xml', 330000],
        ['Smoke_GGPM_DTVR_TEST_20sec.xml', 330000]
    ],
    pauseTimeout: [
        ['PauseTimeout_GGPM_DCRVideo_Preroll_PauseTimeoutEvent7.xml', 100000],
        ['PauseTimeout_GGPM_DCRVideo_Preroll_PauseTimeoutEvent7LessThanPTO.xml', 80000],
        ['PauseTimeout_GGPM_DCRVideo_Preroll_PauseTimeoutEvent6.xml', 100000],
        ['PauseTimeout_GGPM_DCRVideo_Preroll_PauseTimeoutEvent6lessPTO.xml', 80000],
        ['PauseTimeout_GGPM_DCRVideo_preroll_greaterThanPTOEvent7.xml', 105000],
        ['PauseTimeout_GGPM_DCRVideo_preroll_lessThanPTOEvent7.xml', 75000],
        ['PauseTimeout_GGPM_DCRVideo_PrerollPostroll_PauseTimeoutEvent7.xml', 115000],
        ['PauseTimeout_GGPM_DCRVideo_PrerollPostroll_PauseTimeoutEvent7LessThanPTO.xml', 90000],
        ['PauseTimeout_GGPM_DCRVideo_PrerollPostroll_PauseTimeoutEvent6.xml', 115000],
        ['PauseTimeout_GGPM_DCRVideo_PrerollPostroll_PauseTimeoutEvent6lessthanPTO.xml', 95000],
        ['PauseTimeout_GGPM_DCRVideo_PrerollPostroll_PauseTimeoutEvent7GreaterthenPTO.xml', 115000],
        ['PauseTimeout_GGPM_DCRVideo_PrerollContentPauseTimeoutEvent7lessthenPTO1.xml', 92000],
        ['PauseTimeout_GGPM_DCRVideo_PrerollContentPauseTimeout.xml', 115000],
        ['PauseTimeout_GGPM_DCRVideo_PrerollContentNOPauseTimeout.xml', 100000],
        ['PauseTimeout_GGPM_DCRVideo_PrerollContentNOPauseTimeout_Event6.xml', 100000],
        ['PauseTimeout_GGPM_DCRVideo_PrerollContentPauseTimeout_Event6.xml', 110000],
        ['PauseTimeout_GGPM_DCRVideo_PrerollContentPauseTimeoutEvent7.xml', 115000],
        ['PauseTimeout_GGPM_DCRVideo_Midroll_LessThanPTO.xml', 95000],
        ['PauseTimeout_GGPM_DCRVideo_Midroll_greaterThanPTO.xml', 115000],
        ['PauseTimeout_GGPM_DCRVideo_Midroll_LessThanPTOevent6.xml', 95000],
        ['PauseTimeout_GGPM_DCRVideo_Midroll_greaterThanPTOevent6.xml', 115000],
        ['PauseTimeout_GGPM_DCRVideo_Midroll_LessThanPTOevent7.xml', 95000],
        ['PauseTimeout_GGPM_DCRVideo_Midroll_greaterThanPTOevent7.xml', 120000]
    ],
    dtvrAqhGgpm: [
        ['AQH_GGPM_DCR_DTVR_DCR_Hybrid_FD_event57.xml', 3300000],
        ['AQH_GGPM_DCR_DTVR_DCR_Hybrid_FD_event7.xml', 3300000],
        ['AQH_GGPM_DCR_DTVR_DCR_Hybrid_FD_NOevent57.xml', 3300000],
        ['AQH_GGPM_DCR_DTVR_DCR_Hybrid_FDChannelChange_57.xml', 3300000],
        ['AQH_GGPM_DCR_DTVR_DCR_Hybrid_PC_event57.xml', 3300000],
        ['AQH_GGPM_DCR_DTVR_DCR_Hybrid_PCFD_event57.xml', 3300000],
        ['AQH_GGPM_DTVR_FDChannelChange_57.xml', 3300000],
        ['AQH_GGPM_DTVR_PC1FD1_PC2FD1_AQHnotMet.xml', 3300000],
        ['AQH_GGPM_DTVR_PC1FD1_PC2FD1_event15_57.xml', 3300000],
        ['AQH_GGPM_DTVR_PC1FD1_PC2FD1_event15.xml', 3300000],
        ['AQH_GGPM_DTVR_PC1FD1_PC2FD1_event57.xml', 3300000],
        ['AQH_GGPM_DTVR_PC1FD1_PC2FD1.xml', 3300000],
        ['GGpm_mTVR_AQH_fast_forward_within_Quater.xml', 3300000]
    ],
    hybridNoHarmGgpm: [
        ['GGPM_DCR_AllTypesOfAds_WithDifferentAssestId.xml', 10000000],
        ['Smoke_GGPM_DCR_Video_305secs.xml', 10000000],
        ['Dcr_forward_30secsAndScrubTo420.xml', 10000000],
        ['GGPM_AGF_Rewind_to_1st_minute.xml', 10000000],
        ['GGPM_DCR_content_static_Replay.xml', 10000000],
        ['DCR_Adloadtype_Pausetimeout.xml', 10000000],
        ['11_DCR_content_20seconds.xml', 10000000],
        ['GGPM_PCFD_300SECS_withoutEvent57.xml', 10000000],
        ['DCR_static_305seconds.xml', 10000000],
        ['DCR_static_60seconds_optout_init.xml', 10000000],
        ['GGPM_AllParam_CallingEvent5_AfterEvent7AndEvent6AndEvent57.xml', 10000000],
        ['GGPM_AllParam_InsideAdTypeInvalid.xml', 10000000],
        ['GGPM_AllParam_ContentBlankAssetid_AdsInvalidAssetId_Static.xml', 10000000],
        ['Ggpm_DTVR_WithEvent57InBetween.xml', 10000000],
        ['GGPM_DCR_DTVR_PCFD_Event57_InBetween.xml', 10000000],
        ['GGPM_DCR_content_static_Replay', 10000000]
    ],
    hybridNoHarmTrackEvent: [
        ['0_BSDK_Case_Test%20-%20TrackEvent%20-%20DCR%20Video%20with%20ads.xml', 10000000],
        ['TrackEvent_BSDK_DCR_Content_3_minutes.xml', 10000000],
        ['Trackevent_BSDK_DCR_Forward_60secs.xml', 10000000],
        ['trackEvent_BSDK_DCR_Content_2Mins_rewind.xml', 10000000],
        ['trackEvent_BSDK_DCR_Content_Replay.xml', 10000000],
        ['Trackevent_DCR_VOD_OTTOptoutCastedToNonCasted.xml', 10000000],
        ['trackeventpcfdchannelchangemultipleinstance.xml', 10000000],
        ['TrackEvent_BSDK_DCR_multiinstance_2static_video_updated.xml', 10000000],
        ['TrackEvent_BSDK_DTVR_SDK_Browser_MTVR_Live_RepeatedMinsAQH.xml', 10000000],
        ['TrackEvent_AllParam_InsideAdTypeInvalid_NoId3Wait_test2_Richard.xml', 10000000],
        ['TrackEvent_AllParam_AdEndwithComplete.xml', 10000000],
        ['TrackEvent_AGF_AllParam_AllAds_noAssetIdForContent.xml', 10000000],
        ['Trackeventdtvrcompletinbetwwen.xml', 10000000],
        ['Trackevent_Hybrid_case_DCR_DTVR_AppID_DTVR.xml', 10000000]
    ]    
}
