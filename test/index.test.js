const puppeteer = require('puppeteer');
const delay = require('await-delay');
const forEach = require('mocha-each');
const _ = require('underscore');
const testCases = require('../testcase-list.js');
const validatePing = require('../validate-pings.js');
const baseFunction = require('../base-functions');
const args = require('yargs').argv;

const { description, testDescription, testsToRun, testFolder} = generateTestParams(args.testType);

describe(description, function () {
	let browser;

	after('close browser', async function () {
		await browser.close();
		console.log('Test complete');
	});

	forEach(testsToRun)

		.it(testDescription, async function (testName, testDuration) {
				let output = [];
				let testEnvOptions = '';
				let page;
				let pages;

				browser = await puppeteer.launch({ headless: true });
				pages = await browser.pages();
				page = pages[0];
				await page.setRequestInterception(true);

				page.on('request', request => {
					let url = request.url(), obj;
					obj = baseFunction.urlToJson(url);
					if (obj && Object.keys(obj).length > 0 && url.includes('mtvr') || url.includes('trans') || url.includes('tsv') || url.includes('cgi-bin/gn')) {
						request.continue();
						output.push(obj);
					} else { request.continue(); }
				});

				testEnvOptions = await baseFunction.checkEnvOptions(testName);
				await page.goto(testEnvOptions), { waitUntil: 'domcontentloaded' };
				await delay(4000);
				await baseFunction.waitTestComplete(page, testDuration);
				await validatePing.validateTestResult(output, testName, testEnvOptions, testFolder);
				await browser.close();
		})
});

function generateTestParams(testType) {
	switch(testType) {
		case 'pingtest':
			return {
				description: 'BSDK Regression testing',
				testDescription: 'Testing %s for %d milliseconds',
				testsToRun: testCases.newTests,
				testFolder: 'test/test'
			}
		case 'smokedcrv':
			return {
				description: 'BSDK DCR Video Smoke Test Cases',
				testDescription: 'DCR Video Smoke test case %s for %d milliseconds',
				testsToRun: testCases.dcrvideoSmokeTest,
				testFolder: 'bsdk-smoke-tests/dcrvideosmoke'
			}
		case 'smokedcrs':
			return {
				description: 'BSDK DCR Static Smoke Test Cases',
				testDescription: 'DCR Static Smoke test case %s for %d milliseconds',
				testsToRun: testCases.dcrstaticSmokeTest,
				testFolder: 'bsdk-smoke-tests/dcrstaticsmoke'
			}
		case 'smokedtvr':
			return {
				description: 'BSDK DTVR Smoke Test Cases',
				testDescription: 'DTVR Smoke test case %s for %d milliseconds',
				testsToRun: testCases.dtvrSmokeTest,
				testFolder: 'bsdk-smoke-tests/dtvrsmoke'
			}
		case 'rtvod':
			return {
				description: 'BSDK DTVR GGPM RTVOD Regression testing',
				testDescription: 'Testing %s for %d milliseconds',
				testsToRun: testCases.dtvrGgpmRtvodRegression,
				testFolder: 'regression-tests/dtvr-ggpm-rtvod-regression'
			}
		case 'rtvod-rwd-ff':
			return {
				description: 'BSDK DTVR GGPM RTVOD Rewind/Fast-Forward Regression testing',
				testDescription: 'Testing %s for %d milliseconds',
				testsToRun: testCases.dtvrGgpmRtvodFwdFfRegression,
				testFolder: 'regression-tests/dtvr-ggpm-rtvod-fwd-ff'
			}
		case 'rtvod-ch-change':
			return {
				description: 'BSDK DTVR GGPM RTVOD Channel Change Regression testing',
				testDescription: 'Testing %s for %d milliseconds',
				testsToRun: testCases.dtvrGgpmRtvodChannelChangeRegression,
				testFolder: 'regression-tests/dtvr-ggpm-rtvod-channelchange'
			}
		case 'dtvr-ggpm-regression':
			return {
				description: 'BSDK DTVR GGPM Regression testing',
				testDescription: 'Testing %s for %d milliseconds',
				testsToRun: testCases.dtvrGgpmRegression,
				testFolder: 'regression-tests/dtvr-ggpm'
			}
		case 'dtvr-te-regression':
			return {
				description: 'BSDK DTVR TrackEvent Regression testing',
				testDescription: 'Testing %s for %d milliseconds',
				testsToRun: testCases.dtvrTrackEventRegression,
				testFolder: 'regression-tests/dtvr-trackevent-regression'
			}
		default:
			return;
	}
}