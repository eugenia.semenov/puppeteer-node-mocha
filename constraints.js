//WIP, will be done in February, 2020

const fs = require('fs');

// ValidateJS constraint options.
// ------------------------------
const rules = {
	_global: {
		ci: {
			allowEmpty: false,
			message: 'is missing.'
		}
	},
	gn: {
		prd: {
			allowEmpty: false,
			message: 'is missing.'
		},
		uoo: {
			allowEmpty: false,
			message: 'is missing.'
		},
		sessionId: {
			allowEmpty: false,
			message: 'is missing.',
			inclusion: {
				within: ['10', '11', '12'],
				message: 'has an invalid value of %{value}.'
			}
		},
		adid: {
			allowEmpty: false,
			message: 'is missing.'
		},
		davty: {
			allowEmpty: false,
			message: 'is missing.',
			inclusion: {
				within: ['10', '11', '12'],
				message: 'has an invalid value of %{value}.'
			}
		}
	},
	d: {
		c1: {
			allowEmpty: false,
			message: 'is missing.'
		},
		c27: {
			allowEmpty: false,
			message: 'is missing.'
		},
		sessionId: {
			allowEmpty: false,
			message: 'is missing.'
		}
	},
	m: {
		c29: {
			allowEmpty: false,
			message: 'is missing.'
		}
	},
	trans: {
	},
	default: {
		//c27: {custom: ""}  // Trigger a custom validation
	}
};

// Sample of a custom validation function.
//
// validate.validators.custom = function(value, options, key, attributes) {
  // console.log(value);
  // console.log(options);
  // console.log(key);
  // console.log(attributes);
  // return "is totally wrong";
// };

module.exports = {
	get: function(id) {
		id = (id || 'gn');
		return rules[id];
	}
}